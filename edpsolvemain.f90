program edpmain !main program
use iso_fortran_env, only: real64
    use, intrinsic :: iso_fortran_env, only : input_unit, &
                                              output_unit, &
                                              error_unit
use matrix_class
use datainput
use matrix_system
use qr_factorization
use iomodule
!use matrix_remonte included in matrix_class
implicit none
!integer, parameter :: dp = selected_real_kind(15, 307) 
integer, parameter :: dp = selected_real_kind(15, 307)
integer::n,m,ntotales,mtotales,i
real(real64)::c1,c2,tetha,x0,xf,t0,tf,Te0,Tef,k,h,f(3)
type(matrix)::B,Al,C,A1,A2,Q,R
!real(kind=dp),allocatable::mallax(:),fx0(:),C(:,:),Al(:,:),A1(:,:),A2(:,:)
real(real64),allocatable::arraynohom(:),fx0(:),mallax(:),T(:),bs(:),Tlast(:)
integer::j,ti
!array de construccion de B
f=(/1,-2,1/)
call defvar(c1,c2,tetha,x0,xf,t0,tf,Te0,Tef,n,m,ntotales,mtotales,mallax,fx0)
call mat_heat_B(f,ntotales,B)


! Calculo pasos
k=(tf-t0)/(m+1)
h=(xf-x0)/(n+1)

call mat_heat_nonhom(k,c2,tetha,ntotales,arraynohom)

call mat_heat_Al(ntotales,Al)
call mat_heat_C(ntotales,C)
call mat_heat_system(A1,A2,k,tetha,c1,h,B,c2,Al,C,ntotales)

!Boundary terms, constant temperature boundary nodes
call set(A1,1,1,1._real64)
call set(A1,ntotales,ntotales,1._real64)
call set(A2,1,1,1._real64)
call set(A2,ntotales,ntotales,1._real64)






!solving
!iniciales
allocate(T(ntotales),bs(ntotales),Tlast(ntotales))
Tlast=matmul(A2,fx0)
Tlast(1)=Te0
Tlast(size(Tlast))=Tef
call qr(A1,Q,R)

do ti=1,mtotales
    bs=matmul(A2,Tlast)+arraynohom
    call traspose_square(Q)
    T=remonte(R,matmul(Q,bs))
    call traspose_square(Q)
    Tlast=T
   Tlast(1)=Te0
    Tlast(size(Tlast))=Tef
    
end do

call T_output(T)



end program

