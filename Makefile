all: matrix_class.o qr_factorization.o edpinputdata.o edpmatrixsystem.o iomodule.o edpsolvemain.out 

FC=gfortran
FLAGS=-Wall -g
LIBFLAGS=-c

matrix_class.o : matrix_class.f90
	$(FC) $(FLAGS) $(LIBFLAGS) -o $@ $^

qr_factorization.o : qr_factorization.f90
	$(FC) $(FLAGS) $(LIBFLAGS) -o $@ $^

edpinputdata.o : edpinputdata.f90
	$(FC) $(FLAGS) $(LIBFLAGS) -o $@ $^

edpmatrixsystem.o : edpmatrixsystem.f90
	$(FC) $(FLAGS) $(LIBFLAGS) -o $@ $^

iomodule.o : iomodule.f90
	$(FC) $(FLAGS) $(LIBFLAGS) -o $@ $^

edpsolvemain.out : edpsolvemain.f90 matrix_class.o qr_factorization.o edpinputdata.o edpmatrixsystem.o iomodule.f90
	$(FC) $(FLAGS)  -o $@ $^
	rm *.o *.mod 
clean:
	rm *.o *.mod *.out
	



