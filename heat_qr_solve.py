# -*- coding: utf-8 -*-
import subprocess
import numpy as np
import matplotlib.pyplot as plt

fortransolver=subprocess.Popen('./edpsolvemain.out',stdout=subprocess.PIPE)

dataT=fortransolver.communicate()[0]

def data_process(data):
	data=data.split()
	data=np.array(map(float,data))
	return data
Temp=data_process(dataT)


fig = plt.figure()
fig.suptitle('Heat equation with sources', fontsize=14, fontweight='bold')

ax = fig.add_subplot(111)
fig.subplots_adjust(top=0.85)
ax.set_title('c2=0.00 c1=2 T0=0.1 Tf=0.1')

ax.set_xlabel('x')
ax.set_ylabel('T')


ax.plot(Temp)


plt.show()
