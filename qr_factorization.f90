! Module for QR factorization using Matrix class.
module qr_factorization
    use iso_fortran_env, only: real64
    use, intrinsic :: iso_fortran_env, only : input_unit, &
                                              output_unit, &
                                              error_unit
    use matrix_class

    implicit none
    contains

    subroutine qr(A, Q, R)
        type(matrix), intent(in) :: A
        type(matrix), intent(out) :: Q, R
        ! type(matrix) :: Identity, aux_matrix
        ! type(matrix) :: diff, diff_t
        type(matrix) :: H

        real(real64), allocatable :: v(:)
        real(real64), allocatable :: fil(:)
        real(real64), allocatable :: Identity(:,:), aux_matrix(:,:)
        real(real64), allocatable :: diff(:,:), diff_t(:,:)
        real(real64) :: nor

        integer :: m, n
        integer :: i, j, k
        integer :: err

        m = size(A, 1)
        n = size(A, 2)

        call eye(Q,m)
        call copy_matrix(A,R)

        do i = 1, n-1, 1
            call eye(H, m)

            ! not necessary to allocate v,
            ! because its automatically allocated when getting the values from R
            v = get(R, [(k, k=i, m)], i)
            nor = norm2(v)

            allocate(fil(m-i+1), stat=err)
            if (err /= 0) write (error_unit, *) "fil: Allocation request denied"

            allocate(diff(1,m-i+1), stat=err)
            if (err /= 0) write (error_unit, *) "diff: Allocation request denied"

            allocate(diff_t(m-i+1,1), stat=err)
            if (err /= 0) write (error_unit, *) "diff_t: Allocation request denied"

            allocate(aux_matrix(m-i+1,m-i+1), stat=err)
            if (err /= 0) write (error_unit, *) "aux_matrix: Allocation request denied"

            allocate(Identity(m-i+1,m-i+1), stat=err)
            if (err /= 0) write (error_unit, *) "Identity: Allocation request denied"

            ! its necessary to initialize elements to zero
            fil = 0
            Identity = 0
            aux_matrix = 0
            ! diff=0
            ! diff_t=0

            fil(1) = nor
            do j = 1, m-i+1, 1
                Identity(j,j) = 1
                diff(1,j) = v(j) - fil(j)
                diff_t(j,1) = v(j) - fil(j)
            end do


            ! Computation of:  I - (2*v2'*v2)./(v2*v2')
            ! aux_matrix = v2'*v2
            aux_matrix = matmul(diff_t,diff)

            ! aux_matrix = matmul(aux_matrix, real(2, real64))
            aux_matrix = 2._real64 * aux_matrix

            ! aux_matrix = (2*v2'*v2)./(v2*v2')
            diff = matmul(diff,diff_t)
            aux_matrix = 1/(diff(1,1)) * aux_matrix

            ! aux_matrix = (I-(2*v2'*v2)./(v2*v2'))
            aux_matrix = Identity - aux_matrix

            do j = i, m, 1
                do k = i, m, 1
                    call set(H, j, k, aux_matrix(j-i+1, k-i+1))
                end do
            end do

            Q = matmul(Q,H)
            R = matmul(H,R)

            ! Free memory
            call dealloc(H)

            if (allocated(Identity)) deallocate(Identity, stat=err)
            if (err /= 0) write (error_unit, *) "Identity: Deallocation request denied"

            if (allocated(aux_matrix)) deallocate(aux_matrix, stat=err)
            if (err /= 0) write (error_unit, *) "aux_matrix: Deallocation request denied"

            if (allocated(diff)) deallocate(diff, stat=err)
            if (err /= 0) write (error_unit, *) "diff: Deallocation request denied"

            if (allocated(diff_t)) deallocate(diff_t, stat=err)
            if (err /= 0) write (error_unit, *) "diff_t: Deallocation request denied"

            if (allocated(v)) deallocate(v, stat=err)
            if (err /= 0) write (error_unit, *) "v: Deallocation request denied"

            if (allocated(fil)) deallocate(fil, stat=err)
            if (err /= 0) write (error_unit, *) "fil: Deallocation request denied"
        end do

    end subroutine qr


end module qr_factorization
