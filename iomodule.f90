module iomodule
use iso_fortran_env, only: real64
    use, intrinsic :: iso_fortran_env, only : input_unit, &
                                              output_unit, &
                                              error_unit
implicit none

!integer,parameter::o_unit=20 instead of stdoutput


contains
subroutine T_output(T)

    real(real64),allocatable,intent(in)::T(:)
    integer::i,n
    
    !open(unit=o_unit,file="T_results.txt",action="write",status="replace")
    n=size(T)
    do i=1,n
    write(*,*)T(i)
    end do
    !close(unit=o_unit)	
    end subroutine

end module

