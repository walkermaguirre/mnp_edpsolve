! Class to use sparse matrices in a efficient way
! Matrices are stored using COO scheme (Coordinate list)
! This way there are only stored the row, the column and the value of each element of the matrix
module matrix_class
    use iso_fortran_env, only: real64
    use, intrinsic :: iso_fortran_env, only : input_unit, &
                                              output_unit, &
                                              error_unit

    implicit none

    ! Definition and class attributes
    type :: matrix
        private
        integer :: n, m
        integer, allocatable :: row(:), col(:)
        real(real64), allocatable :: val(:)
    end type

    interface get !public name outside this module
        module procedure get_sca
        module procedure get_row
        module procedure get_col
    end interface

    interface set
        module procedure set_sca
    end interface

    interface size
        module procedure size_matrix
    end interface

    interface matmul
        module procedure matmul_matrix
        module procedure mat_scalar_prod
        module procedure matrix_multiplication
    end interface

    private :: get_sca, get_col, get_row, size_matrix, set_sca, matmul_matrix ! this methods are hidden outside this module

    contains
    ! Class method


    ! Returns the value stored in the position i,j of the matrix.
    ! If it does not exists the functions returns 0 (zero).
    function get_sca(this, i, j) result(val)
        type(matrix), intent(in) :: this
        integer, intent(in) :: i, j
        real(real64) :: val

        integer :: k

        if ( allocated(this%row) .AND. allocated(this%col) .AND. allocated(this%val) ) then
            do k = 1, size(this%row, 1)
                if(this%row(k) == i .and. this%col(k) == j) then
                    val = this%val(k)
                    return
                end if
            end do
        end if

        ! if the value is not found or arrays are not allocated, zero is returned
        val = 0._real64
    end function

    ! Stores a value in the position i,j of the matrix.
    subroutine set_sca(this, i, j, val)
        type(matrix), intent(inout) :: this
        real(real64), intent(in) :: val
        integer, intent(in) :: i, j

        integer :: s, k
        integer, allocatable :: trow(:), tcol(:)
        real(real64), allocatable :: tval(:)

        ! if introducing a zero or values near epsilon, store nothing (just update matrix size)
        ! if the value already exists, its updated to 0.0
        if ( abs(val) <= 1D-15 ) then
            if (allocated(this%row) .and. allocated(this%col) .and. allocated(this%val)) then
                s = size(this%row, 1)
                do k = 1, s
                    if (this%row(k) == i .and. this%col(k) == j) then
                        this%val(k) = 0._real64
                        return
                    end if
                end do
            end if

            this%n = max(this%n, i)
            this%m = max(this%m, j)
            return
        end if

        if (.not. allocated(this%row) .or. .not. allocated(this%col) .or. .not. allocated(this%val)) then
            call alloc(this, [i], [j], [val])
        else
            s = size(this%row, 1)

            do k = 1, s
                if (this%row(k) == i .and. this%col(k) == j) then
                    this%val(k) = val
                    return
                end if
            end do

            allocate(trow(s+1), tcol(s+1), tval(s+1))
            trow(1:s) = this%row; trow(s+1) = i
            tcol(1:s) = this%col; tcol(s+1) = j
            tval(1:s) = this%val; tval(s+1) = val

            ! reallocs this%row with the width and values of trow
            call move_alloc(trow, this%row)
            call move_alloc(tcol, this%col)
            call move_alloc(tval, this%val)

            ! update size of matrix
            this%n = max(this%n, i)
            this%m = max(this%m, j)
        end if
    end subroutine

    ! Returns the elements of a sub-column
    function get_col(this, i, j) result(val)
        type(matrix), intent(in) :: this
        integer, intent(in) :: i, j(:)
        real(real64) :: val(size(j,1))
        integer :: k

        if (size(j, 1) < 0) then
            write (error_unit, *) 'ERROR (matrix/get_sca) Insufficient number of indices.'
            stop 1
        end if
        do k = 1, size(j,1)
            val(k) = get(this, i, j(k))
        end do
    end function

    ! Returns the elements of a sub-row
    function get_row(this, i, j) result(val)
        type(matrix), intent(in) :: this
        integer, intent(in) :: i(:), j
        real(real64) :: val(size(i,1))
        integer :: k

        if (size(i, 1) < 0) then
            write (error_unit, *) 'ERROR (matrix/get_sca) Insufficient number of indices.'
            stop 1
        end if
        do k = 1, size(i,1)
            val(k) = get(this, i(k), j)
        end do
    end function

    ! Returns the selected dimension of the matrix
    function size_matrix(this, d) result(res)
        type(matrix) :: this
        character(255) :: cad
        integer :: d, res

        select case (d)
            case(1)
                res = this%n
            case(2)
                res = this%m
            case default
                write (cad, *) d
                write (error_unit, *) 'ERROR (matrix/size_matrix) The requested dimension is invalid: '//trim(adjustl(cad))
                stop 1
        end select
    end function



    ! Returns the result of matrix * vector
    function matmul_matrix(this, x) result(y)
        type(matrix) :: this
        real(real64) :: x(:)
        real(real64), allocatable :: y(:)

        integer :: m, n, i, j, err
        character(255) :: cad


        if (.not. allocated(this%row) .or. .not. allocated(this%col) .or. .not. allocated(this%val)) then
            write (error_unit, *) 'ERROR (matrix/matmul_matrix) Matrix is not allocated.'
            stop 1
        end if

        n = size(this,1)
        m = size(this,2)
        if (m /= size(x,1)) then
            write (error_unit, *) 'ERROR (matrix/matmul_matrix) The matrix is not compatible with x.'
            stop 1
        end if

        if (n < 0) then
            write (cad, *) n
            write (error_unit, *) 'ERROR (matrix/matmul_matrix) The number of rows is invalid: '//trim(adjustl(cad))
            stop 1
        end if

        allocate(y(n), stat=err)
        if (err /= 0) write (error_unit, *) "y(:): Allocation request denied"

        do i = 1, n
            y(i) = dot_product(get(this, i, [(j, j = 1, m)]), x)
        end do
    end function

    ! Returns the result of matriz * escalar.
    ! Multiplies each element of the matrix by a escalar
    function mat_scalar_prod (this, x) result (y)
        type(matrix) :: this, y
        real(real64) :: x, val
        integer :: i, j

        integer :: rows, cols

        rows = size(this,1)
        cols = size(this,2)

        do i = 1, cols, 1
            do j = 1, rows, 1
                val = get(this,i,j) * x
                call set(y,i,j,val)
            end do
        end do
    end function mat_scalar_prod

    ! Returns the result of matriz / escalar.
    ! Divides each element of the matrix by a escalar
    function mat_scalar_div (this, x) result (y)
        type(matrix) :: this, y
        real(real64) :: x, val
        integer :: i, j

        integer :: rows, cols

        rows = size(this,1)
        cols = size(this,2)

        do i = 1, cols, 1
            do j = 1, rows, 1
                val = get(this,i,j) / x
                call set(y,i,j,val)
            end do
        end do
    end function mat_scalar_div

    ! Returns the result of matrix multiplication
    function matrix_multiplication(a, b) result(c)
        type(matrix), intent(in) :: a, b
        type(matrix) :: c

        integer :: i, j, k
        integer :: rows_a, cols_a, cols_b

        real(real64) :: aux

        rows_a = size(a,1)
        cols_a = size(a,2)
        cols_b = size(b,2)

        do i = 1, rows_a, 1
            do j = 1, cols_b, 1
                aux = 0.
                do k = 1, cols_a, 1
                    aux = aux + get_sca(a,i,k) * get_sca(b,k,j)
                end do
                call set_sca(c,i,j,aux)
            end do
        end do
    end function matrix_multiplication

    ! Returns the result of substraction of matrices
    function matrix_add(a, b) result (c)
        type(matrix) :: a, b, c
        integer :: i, j
        integer :: rows, cols
        real(real64) :: aux

        ! if ( (size(a,1) .NE. size(b, 1)) .OR. (size(a,2) .NE. size(b,2)) ) then
        !     write (error_unit, *) "Error, not equal dimensions of matrices"
        !     return
        ! end if

        rows = size(a,1)
        cols = size(a,2)

        do j = 1, cols, 1
            do i = 1, rows, 1
                aux = get(a,i,j) + get(b,i,j)
                call set_sca(c,i,j,aux)
            end do
        end do
    end function matrix_add

    ! Returns the result of substraction of matrices
    function matrix_sub(a, b) result (c)
        type(matrix), intent(in) :: a, b
        type(matrix) :: c
        integer :: i, j
        integer :: rows, cols
        real(real64) :: aux

        ! if ( (size(a,1) .NE. size(b, 1)) .OR. (size(a,2) .NE. size(b,2)) ) then
        !     write (error_unit, *) "Error, not equal dimensions of matrices"
        !     return
        ! end if

        rows = size(a,1)
        cols = size(a,2)

        do j = 1, cols, 1
            do i = 1, rows, 1
                aux = get(a,i,j) - get(b,i,j)
                call set_sca(c,i,j,aux)
            end do
        end do
    end function matrix_sub



    ! Allocate memory and initialize the matrix
    subroutine alloc(this, row, col, val)
        type(matrix), intent(inout) :: this
        integer, intent(in) :: row(:), col(:)
        real(real64), intent(in) :: val(:)
        character(255) :: cad

        !check size
        if (size(row,1) /= size(col,1) .or. size(row,1) /= size(val,1)) then
            write (error_unit, *) 'ERROR (matrix/alloc) The three input arrays have different size.'
            stop 1
        end if
        if (size(row,1) <= 0) then
            write(cad,*) size(row,1)
            write (error_unit, *) 'ERROR (matrix/alloc) The input arrays have an invalid size: '//trim(adjustl(cad))
            stop 1
        end if

        ! allocation of row, col and val
        if (allocated(this%row) .or. allocated(this%col) .or. allocated(this%val)) then
            call dealloc(this)
        end if
        allocate(this%row(size(row,1)), this%col(size(row,1)), this%val(size(row,1)))
        ! initialize attributes
        this%row = row
        this%col = col
        this%val = val
        this%n = maxval(row)
        this%m = maxval(col)
    end subroutine alloc

    ! Deallocate memory and destroy the matrix
    subroutine dealloc(this)
        type(matrix), intent(inout) :: this
        integer :: err

        this%n = 0
        this%m = 0

        if (allocated(this%row)) deallocate(this%row, stat=err)
        if (err /= 0) write (error_unit, *) "this%row: Deallocation request denied"

        if (allocated(this%col)) deallocate(this%col, stat=err)
        if (err /= 0) write (error_unit, *) "this%col: Deallocation request denied"

        if (allocated(this%val)) deallocate(this%val, stat=err)
        if (err /= 0) write (error_unit, *) "this%val: Deallocation request denied"
    end subroutine dealloc

    ! Initialize the matrix as the identity matrix of order n
    subroutine eye(this, n)
        type(matrix), intent(inout) :: this
        integer, intent(in) :: n
        integer :: i

        do i = 1, n, 1
            call set(this, i, i, real(1, real64))
        end do
    end subroutine eye

    ! Initialize the matrix with zeros
    subroutine zeros(this, n)
        type(matrix), intent(inout) :: this
        integer :: n, i

        this%n = n
        this%m = n

        do i = 1, n, 1
            call set(this, i, i, real(0, real64))
        end do
    end subroutine zeros

    ! Copies the content of src matrix to the dst matrix
    subroutine copy_matrix (src, dst)
        type(matrix), intent(in) :: src
        type(matrix), intent(inout) :: dst
        integer :: i, j
        integer :: rows, cols

        rows = size(src,1)
        cols = size(src,2)

        do j = 1, cols, 1
            do i = 1, rows, 1
                call set_sca(dst,i,j,get(src,i,j))
            end do
        end do
    end subroutine copy_matrix



    ! Metodo para imprimir la matriz
    subroutine show(this)
        type(matrix), intent(in)  :: this
        integer :: i, j

        do i = 1, this%n
            print*, (get(this, i, j), j = 1, this%m)
        end do
    end subroutine show
 subroutine traspose_square(this) 
        type(matrix),intent(inout)::this
        integer:: i, j,n
        real(real64)::val1,val2
        n=size(this,1)
        do i=1,n
        do j=i,n
        val1=get(this,i,j)
        val2=get(this,j,i)
        call set(this,i,j,val2)
        call set(this,j,i,val1)
        end do
        end do
     end subroutine traspose_square
     
     
     function remonte(A,bs) result(x)
            type(matrix),intent(in)::A
            real(real64),intent(in)::bs(:)
            real(real64), allocatable::x(:)
            real(real64)::sum2,aval
            integer :: i,j,n,ir
            n=size(bs)
            !A=[1,7,3;0,4,5;0,0,6];
            !b=[1,2,3];
            allocate(x(n))

            ir=size(bs); !reverse index

            x(n)=bs(n)/get(A,n,n);

            do i=1,(n-1)
                ir=n-i
                sum2=0
                do j=(ir+1),n
                sum2=sum2+get(A,ir,j)*x(j)
                end do
                x(ir)=(bs(ir)-sum2)/get(A,ir,ir)
                end do
                end function

end module
